import {Routes} from '@angular/router'
import { AboutComponent } from './about/about.component'
import { InitializeComponent } from './assinatura/assinatura.component'

export const ROUTES: Routes = [
    {path: '', component: InitializeComponent},
    {path: 'about', component: AboutComponent}
]
