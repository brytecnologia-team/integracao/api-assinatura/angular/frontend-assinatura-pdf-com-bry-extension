import { Component, OnInit, Input } from '@angular/core';
import { Router } from "@angular/router";
import {FormGroup, FormBuilder, FormControl, Validators, AbstractControl} from '@angular/forms'
import {SignatureService} from './assinatura.service'
import {tap} from 'rxjs/operators'
declare var BryApiModule: any;

interface Signature {
  hashes: string[];
  content?: string; // Assuming 'content' exists in the `result.assinaturas[0]`.
}

interface Assinatura {
  nonce: string;
  hashes: string[];
  content: string; // Add other relevant fields if needed.
}

interface Response {
  formatoDadosEntrada: string;
  formatoDadosSaida: string;
  algoritmoHash: string;
  assinaturas: Signature[];
}

interface Result {
  signedAttributes: Signature[];
  assinaturas: Assinatura[];
}

@Component({
  selector: 'cms-assinatura',
  templateUrl: './assinatura.component.html',
  styleUrls: ['./assinatura.component.css']
})
export class InitializeComponent implements OnInit {


  signatureForm: FormGroup;

  inicializationResult;

  finalizationResult;

  file: any;

  imageUpload: any;

  signedAttributes;

  initializedDocument;

  base64Certificate;

  title =  'PDF Signature';

  auxSignatureValue;

  signature;

  token;

  nonces;

  constructor(private signatureService: SignatureService,
              private router: Router,
              private formBuilder: FormBuilder) {

  }

  ngOnInit() {

    this.signatureForm = this.formBuilder.group({
      token:  new FormControl('', { validators: [Validators.required], updateOn: 'blur'}),
      profile: new FormControl('BASIC', { validators: [Validators.required], updateOn: 'blur'}),
      hashAlgorithm: new FormControl('SHA256', { validators: [Validators.required], updateOn: 'blur'}),
      certificate: new FormControl(''),
      file: this.formBuilder.control(null, [Validators.required]),
      image: this.formBuilder.control(''),
      height: new FormControl(60),
      width: new FormControl(170),
      position: new FormControl('INFERIOR_ESQUERDO'),
      page: new FormControl('PRIMEIRA'),
      signature: new FormControl('')
    })

  }

  initialize(signatureForm: FormGroup, base64Certificate) {

    this.token = this.signatureForm.get('token').value;

    this.base64Certificate = base64Certificate;

    this.nonces = `PDF${this.signatureService.generateRandonNumber()}`;
    console.log('Nonces', this.nonces);

    this.signatureService.initialize(this.nonces, signatureForm, this.base64Certificate, this.file, this.imageUpload, this.token)
      .pipe(
        tap((result: any) => {
          this.inicializationResult = result;
        }
        )
      ).subscribe( async (result: any) => {

        console.log(`Inicialization result sem o parse: ${JSON.stringify(result)}`);
        console.log(result);
        let nonceRequisicao = result.nonce;

        
        const response = {
          algoritmoHash: "SHA256",
          formatoDadosEntrada: "Base64",
          formatoDadosSaida: "Base64",
          assinaturas: result.assinaturasInicializadas.map((signature: any) => {
            // Directly assign the messageDigest from the first element to hashes
            const hashes = (signature.messageDigest) ? [signature.messageDigest] : [""]; // Use messageDigest from the signature
            
            return {
              hashes, // Add the hashes array to each signature
            };
          }),
        };
      
        const element = document.getElementById("select-certificado-list") as HTMLSelectElement | null;

        let signedData;

        await BryApiModule.sign(element.value, JSON.stringify(response))
        .then(async signature => {
          signedData = signature
        });

        console.log('Encrypted signed attributes and Base64 encoded: ', signedData.assinaturas[0].hashes[0]);

        let signedAttributes = signedData.assinaturas[0].hashes[0];

        this.signatureService.finalize(nonceRequisicao, this.nonces, signedAttributes, this.token)
            .pipe(
                 tap((result: any) => {
                  this.finalizationResult = result;
                  }
            )
        ).subscribe((result: any) => {

          console.log(`Finalization result: ${JSON.stringify(result)}`);

          this.signature = JSON.stringify(result)

          this.signatureForm.get('signature').setValue(this.signature);

          console.log('link',result.documentos[0].links[0].href );

          let signatureID = result.identificador;

          this.signatureService.getSignature(result.documentos[0].links[0].href).subscribe((result: any) =>{
            let blob = new Blob([result], {type: 'application/pdf'});

            var downloadURL = window.URL.createObjectURL(blob);
            var link = document.createElement('a');
            link.href = downloadURL;
            link.download = `signature-${signatureID}.pdf`;
            link.click();
            alert('Signature realized successful!');
          });

        })

      } )

  }

  clear(){
    this.signature = null;
    this.file = null;
    this.imageUpload = null;
    this.signatureForm.get('image').reset();
    this.signatureForm.get('image').setValue(null);
    this.signatureForm.get('file').reset()
    this.signatureForm.get('file').setValue(null);
    this.signatureForm.get('hashAlgorithm').setValue('SHA256');
  }


  fillCertificateDataForm() {
    BryApiModule.fillCertificateDataForm();
  }

  listCertificates() {
    BryApiModule.listCertificates();
  }

  async signDataByExtension(prepareInputData) {
    const signedData = BryApiModule.sign(prepareInputData);
    return signedData;
  }


  upload(event){
    if (event.target.files.length > 0) {
      this.file = event.target.files[0];

    }
  }


  uploadImage(event){
    if (event.target.files.length > 0) {
      this.imageUpload = event.target.files[0];
    }else{
      this.imageUpload = null;
    }
  }


}
