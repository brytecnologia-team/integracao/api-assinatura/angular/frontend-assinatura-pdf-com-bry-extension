import {Injectable} from '@angular/core'
import {HttpClient, HttpHeaders} from '@angular/common/http'
import {Observable} from 'rxjs'
import {map} from 'rxjs/operators'
import {FormGroup} from '@angular/forms'


@Injectable()
export class SignatureService{

    URL_INITIALIZE_PDF_SERVER  = '/api/initialize';
    URL_FINALIZE_PDF_SERVER  = '/api/finalize';

    constructor(private http: HttpClient){}


    initialize(nonces, assinaturaForm: FormGroup, base64Certificate, file, imageUpload, token): Observable<any>{
        let headersValues = new HttpHeaders();
        headersValues = headersValues.set('Authorization', token)
        const formData = new FormData();
        formData.append('nonces', nonces);
        formData.append('perfil', assinaturaForm.get('profile').value );
        formData.append('algoritmoHash', assinaturaForm.get('hashAlgorithm').value );
        formData.append('certificado', base64Certificate );
        formData.append('formatoDadosEntrada', 'Base64' );
        formData.append('formatoDadosSaida', 'Base64' );
        formData.append('documento', file );
        if(imageUpload){
          formData.append('imagem', imageUpload );
          formData.append('altura', assinaturaForm.get('height').value );
          formData.append('largura', assinaturaForm.get('width').value );
          formData.append('posicao', assinaturaForm.get('position').value );
          formData.append('pagina', assinaturaForm.get('page').value );

        }

        return this.http.post<any>(this.URL_INITIALIZE_PDF_SERVER, formData, {headers: headersValues}).pipe(map( result => result));
    }

    finalize(nonceRequisicao, noncesPDF, signatureValue, token): Observable<any>{
        let headersValues = new HttpHeaders();
        headersValues = headersValues.set('Authorization', `Bearer ${token}`);
        const formData = new FormData();
        formData.append('nonce', nonceRequisicao);
        formData.append('nonces', noncesPDF);
        formData.append('assinaturasPkcs1', signatureValue );
        formData.append('formatoDeDados', 'Base64' );

        return this.http.post<any>(this.URL_FINALIZE_PDF_SERVER, formData, {headers: headersValues}).pipe(map( result => result));

    }

    getSignature(link: string) {
      return this.http.get(link, {responseType: 'blob'});
    }

    generateRandonNumber(): number {
        //Return a random number between 1 and 1000:
        return Math.floor((Math.random() * 1000) + 1);
    }


}
